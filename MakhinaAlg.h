#pragma once

#include <vector>

#include "person.h"
#include "Algorithm.h"

class MakhinaAlg: public Algorithm
{
private:
	vector< person > algLearnPeople;

	double distToShadow(person& from, person& to);
	double MDist(person &a, person &b);
	int findNearestLabel(person &p);

public:
	virtual int learn(vector < person > &lPeople);
	virtual vector< int > test(vector < person > &algTestPeople);
	virtual void report(ofstream *out);
};


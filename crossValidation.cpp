#include <algorithm>
#include <chrono> 
#include <random>

#include "crossValidation.h"
#include "AUC.h"

void crossValidation::report(ofstream* out)
{
	*out << "***Cross validation section***" << endl;

	*out << "Average AUC -- " << AUC << endl;
	*out << "Average error percentage -- " << err << endl << endl;

	*out << "Average learning A_U_C -- " << learnAUC << endl;
	*out << "Average learning e_r_r_o_r percentage -- " << learnErr << endl << endl;

	*out << "---------------------------------------------------------------------------" << endl;
}

void crossValidation::fillPos(vector< int > &pos, int nPeople)
{
	pos.resize(nPeople);
	for (int i = 0; i < nPeople; i++)
		pos.at(i) = i;

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffle(pos.begin(), pos.end(), std::default_random_engine(seed));
}

void pushPeople(ObjFeatureMatrix& from, vector< int >& pos, int &posA, int &posB, int inc, ObjFeatureMatrix &toLearn, ObjFeatureMatrix &toTest)
{
	for (int i = posA; i < posB; i++)
		toTest.pushBack(from[pos[i]], from.getObjectState(pos[i]));

	for (int i = 0; i < posA; i++)
		toLearn.pushBack(from[pos[i]], from.getObjectState(pos[i]));
	for (int i = posB; i < pos.size(); i++)
		toLearn.pushBack(from[pos[i]], from.getObjectState(pos[i]));

	posA = posB;
	posB = min(posB + inc, from.size());
}

void mergeHealthyAndIllPeople(ObjFeatureMatrix &people, ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople)
{
	for (int i = 0; i < healthyPeople.size(); i++)
		people.pushBack(healthyPeople[i]);
	for (int j = 0; j< illPeople.size(); j++)
		people.pushBack(illPeople[j]);
}

void crossValidation::doCrossValidation(Algorithm& alg, ObjFeatureMatrix& healthyPeople, ObjFeatureMatrix& illPeople, int nFolds)
{

	fillPos(posHealthy, healthyPeople.size());
	fillPos(posIll, illPeople.size());

	AUC = 0;
	err = 0;
	learnAUC = 0;
	learnErr = 0;

	nTestHealthy = healthyPeople.size() / nFolds;
	nTestIll = illPeople.size() / nFolds;

	int posHealthyA = 0, posHealthyB = nTestHealthy;
	int posIllA = 0, posIllB = nTestIll;
	for (int iFold = 0; iFold < nFolds; iFold++)
	{
		ObjFeatureMatrix learnHealthyPeople, learnIllPeople;
		ObjFeatureMatrix testPeople;
		ObjFeatureMatrix learnPeople;
	
		learnHealthyPeople.copyFeaturesStates(healthyPeople);
		learnIllPeople.copyFeaturesStates(healthyPeople);
		learnPeople.copyFeaturesStates(healthyPeople);
		testPeople.copyFeaturesStates(healthyPeople);

		pushPeople(healthyPeople, posHealthy, posHealthyA, posHealthyB, nTestHealthy, learnHealthyPeople, testPeople);
		pushPeople(illPeople, posIll, posIllA, posIllB, nTestIll, learnIllPeople, testPeople);
		mergeHealthyAndIllPeople(learnPeople, learnHealthyPeople, learnIllPeople);

		alg.learn(learnHealthyPeople, learnIllPeople);
		
		alg.test(testPeople);
		
		
		vector< pair< double, int > > forAUC;
		err += (double)makeVectorOfPairs(forAUC, testPeople, alg.ans) / (double)testPeople.size();
		double curAUC = countAUC(forAUC);
		AUC += curAUC;
		
		alg.test(learnPeople);
		vector< pair< double, int > > forAUCLearn;
		learnErr +=  (double) makeVectorOfPairs(forAUCLearn, learnPeople, alg.ans) / (double)learnPeople.size();
		learnAUC += countAUC(forAUCLearn);
	}

	err /= nFolds;
	AUC /= nFolds;
	learnErr /= nFolds;
	learnAUC /= nFolds;
}

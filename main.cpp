/**********************************************************************************
	* File: main.cpp
	* Description: 
	* Created: 10.09.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

//TODO: Headline

#include <iostream>
//#include <fstream>

//#include "person.h"
#include "ObjFeatureMatrix.h"
#include "blocks.h"
//#include "NBayes.h"
//#include "dataTransformation.h"
#include "MonotonicNN.h"
#include "crossValidation.h"
#include "NN.h"
//#include "MakhinaAlg.h"
#include "greedyPFS.h"


using namespace std;

#define diseasesNames_file_name "..\\data\\prepared_data\\diseases.txt"
#define healthy_file_name "..\\data\\prepared_data\\��.txt"
char data_path[100] = "..\\data\\prepared_data\\";

// ������ ��������
// ��, ���, ���, ���, ���, ���, ���, ���, ���, ���, ���, ���, ���, ���, ���, ��, ���, ���, ���

// reads diseases list from file diseasesNames_file_name to a array of char* and returns the array
char **readDiseaseList(int &nDiseases)
{
	char **res;
	ifstream input;
	input.open(diseasesNames_file_name);
	input >> nDiseases;
	res = new char*[nDiseases];
	for (int i = 0; i < nDiseases; i++)
	{
		res[i] = new char[20];
		input >> res[i];
	}
	input.close();
	return res;
}

/*
// Main function to test with Ignatov's polygon
#include <string>
int main(int argc, char* argv[]) 
{
	int nFeatures = 5;
	double alpha = 0.3;

	ObjFeatureMatrix learnHealthyPeople;
	ObjFeatureMatrix learnIllPeople;
	ObjFeatureMatrix testPeople;
	
	string paramNFeatures = "number_of_features";
	string paramAlpha = "alpha_value";
	int iArg = 1;
	while (iArg < argc) 
	{
		string a = argv[iArg];
		if (a == paramNFeatures)
			nFeatures = atoi(argv[iArg + 1]);
		else if (a == paramAlpha)
			alpha = atof(argv[iArg + 1]) / 100;
		iArg++;
	}
	
	FILE *fin;
	FILE *fout;
	fopen_s(&fin, "input.txt", "r");
	fopen_s(&fout, "out.txt", "w");
	
	int feat_size, train_size, test_size;
	fscanf_s(fin, "%d%d", &train_size, &test_size);
	
	for (int i = 0; i < train_size; i++)
	{
		fscanf_s(fin, "%d", &feat_size);
		person temp;
		temp.readit(fin, rTypeLabelLast);

		if (temp.getLabel() == Healthy)
			learnHealthyPeople.pushBack(temp);
		else
			learnIllPeople.pushBack(temp);
	}
	
	for (int i = 0; i < test_size; i++)
	{
		fscanf_s(fin, "%d", &feat_size);
		person temp;
		temp.readit(fin, rTypeNoLabel);
		testPeople.pushBack(temp);
	}
	
	fclose(fin);

	// FEATURE SELECTION
	vector< pair< int, double > > weights = getWeights(learnHealthyPeople, learnIllPeople, mGetWeightsOccurrenceDiff, false);
	doPreliminaryFeatureSelection(nFeatures, weights, learnHealthyPeople, learnIllPeople);
	//testPeople.copyFeaturesStates(learnHealthyPeople);

	//
	// CHANGE SIGNS SECTION
	vector< bool > isChanged = doChangeSigns(learnHealthyPeople, learnIllPeople, 0.1, false);	
	for (int k = 0; k < ngramm_vector_size; k++)
		if (isChanged[k])
		{
			for (int i = 0; i < testPeople.size(); i++)
				testPeople[i][k] = -testPeople[i][k] - 0.5;
		}
	
	
	//NBayes myAlg;
	//NN myAlg;
	MonotonicNN myAlg;
	myAlg.setAlpha(alpha);
	myAlg.learn(learnHealthyPeople, learnIllPeople);
	myAlg.test(testPeople);
	//
	
	for (int i = 0; i < test_size; i++)
		fprintf(fout, "%lf ", myAlg.ans[i]);

	fclose(fout);
	return 0; 
}
*/

/*
// 1NN Main function to test with Ignatov's polygon
#include <string>
int main(int argc, char* argv[]) 
{ 
	int nFeatures = 1;

	ObjFeatureMatrix learnHealthyPeople;
	ObjFeatureMatrix learnIllPeople;
	ObjFeatureMatrix testPeople;
	
	string paramNFeatures = "number_of_features";
	int iArg = 1;
	while (iArg < argc) 
	{
		string a = argv[iArg];
		if (a == paramNFeatures)
			nFeatures = atoi(argv[iArg + 1]);
		iArg++;
	}
	
	FILE *fin;
	FILE *fout;
	fopen_s(&fin, "input.txt", "r");
	fopen_s(&fout, "out.txt", "w");
	
	int feat_size, train_size, test_size;
	fscanf_s(fin, "%d%d", &train_size, &test_size);
	
	for (int i = 0; i < train_size; i++)
	{
		fscanf_s(fin, "%d", &feat_size);
		person temp;
		temp.readit(fin, rTypeLabelLast);

		if (temp.getLabel() == Healthy)
			learnHealthyPeople.pushBack(temp);
		else
			learnIllPeople.pushBack(temp);
	}
	
	for (int i = 0; i < test_size; i++)
	{
		fscanf_s(fin, "%d", &feat_size);
		person temp;
		temp.readit(fin, rTypeNoLabel);
		testPeople.pushBack(temp);
	}
	
	fclose(fin);

	
	//NAIVE BAYESIAN FEATURE SELECTION SECTION
	//vector< pair< int, double > > NBayesWeights = getBayesianWeights(learnHealthyPeople, learnIllPeople, false);
	//doPreliminaryFeatureSelection(nFeatures, NBayesWeights, learnHealthyPeople, learnIllPeople);

	NN myAlg;
	myAlg.learn(learnHealthyPeople, learnIllPeople);
	myAlg.test(testPeople);
	//
	
	for (int i = 0; i < test_size; i++)
		fprintf(fout, "%d ", (int)myAlg.ans[i]);

	fclose(fout);
	return 0; 
}
*/


// main function to test my algorithms on Celyh's data (28.10.2014)
int main()
{
	//ofstream out("results\\report.txt");

	int nDiseases;
	char **diseasesList = readDiseaseList(nDiseases);
	
	ofstream out("results\\greedyRreport.txt");
	//int iDisease = 1; 
	for (int iDisease = 0; iDisease < nDiseases; iDisease++)
	{
		out << "Testing disease: " << diseasesList[iDisease] << endl << endl;

		char ill_file_name[50] = "";
		strcat_s(ill_file_name, data_path);
		strcat_s(ill_file_name, diseasesList[iDisease]);
		strcat_s(ill_file_name, ".txt");

		// Reading healthy and ill people
		ObjFeatureMatrix healthyPeople(healthy_file_name, Healthy);
		ObjFeatureMatrix illPeople(ill_file_name, Ill);

		// CHANGE SIGNS SECTION
		doChangeSigns(healthyPeople, illPeople, 0.1, false);
				
		/*
		int nFeatures = 15; 
		//for (int nFeatures = 50; nFeatures > 0; nFeatures--)
		{
			//doPreliminaryFeatureSelection(nFeatures, NBayesWeights, healthyPeople, illPeople);

			//MonotonicNN myAlg;
			//crossValidation CV;
			//const int nFolds = 10;
			//CV.doCrossValidation(myAlg, healthyPeople, illPeople, nFolds); 
			//CV.report(&out);

			//out << nFeatures << " " << CV.AUC << endl;

		}
		*/

		healthyPeople.disableAllFeatures();
		illPeople.disableAllFeatures();

		greedyPFS greedyPFSInstance(healthyPeople, illPeople);
		greedyPFSInstance.runGreedyPFS(2);
		greedyPFSInstance.printReport(out);
	}
	
	//system("pause");
	return 0;
}


/*
// greedy PFS
#include <string>

int main(int argc, char* argv[]) 
{ 
	int nFeatures = 25;

	ObjFeatureMatrix learnHealthyPeople;
	ObjFeatureMatrix learnIllPeople;
	ObjFeatureMatrix testPeople;
	
	string paramNFeatures = "number_of_features";
	int iArg = 1;
	while (iArg < argc) 
	{
		string a = argv[iArg];
		if (a == paramNFeatures)
			nFeatures = atoi(argv[iArg + 1]);
		iArg++;
	}
	
	FILE *fin;
	FILE *fout;
	fopen_s(&fin, "input.txt", "r");
	fopen_s(&fout, "out.txt", "w");
	
	int feat_size, train_size, test_size;
	fscanf_s(fin, "%d%d", &train_size, &test_size);
	
	for (int i = 0; i < train_size; i++)
	{
		fscanf_s(fin, "%d", &feat_size);
		person temp;
		temp.readit(fin, rTypeLabelLast);

		if (temp.getLabel() == Healthy)
			learnHealthyPeople.pushBack(temp);
		else
			learnIllPeople.pushBack(temp);
	}
	
	for (int i = 0; i < test_size; i++)
	{
		fscanf_s(fin, "%d", &feat_size);
		person temp;
		temp.readit(fin, rTypeNoLabel);
		testPeople.pushBack(temp);
	}
	
	fclose(fin);

	//
	// CHANGE SIGNS SECTION
	vector< bool > isChanged = doChangeSigns(learnHealthyPeople, learnIllPeople, 0.1, false);	
	for (int k = 0; k < ngramm_vector_size; k++)
	if (isChanged[k])
	{
		for (int i = 0; i < testPeople.size(); i++)
			testPeople[i][k] = -testPeople[i][k] - 0.5;
	}
	

	learnHealthyPeople.disableAllFeatures();
	learnIllPeople.disableAllFeatures();

	greedyPFS greedyPFSInstance(learnHealthyPeople, learnIllPeople);
	greedyPFSInstance.runGreedyPFS(2);
	ofstream out("results\\greedyRreport.txt");
	greedyPFSInstance.printReport(out);

	MonotonicNN myAlg;
	myAlg.learn(learnHealthyPeople, learnIllPeople);
	myAlg.test(testPeople);
	//
	
	for (int i = 0; i < test_size; i++)
		fprintf(fout, "%lf ", myAlg.ans[i]);

	fclose(fout);
	return 0; 
}
*/


/*
// main function to test my algorithms on Celyh's data (28.10.2014)
int main()
{
	ofstream out("results\\report.txt");

	int nDiseases;
	char **diseasesList = readDiseaseList(nDiseases);
	
	int iDisease = 1; 
	out << "Testing disease: " << diseasesList[iDisease] << endl << endl;

	char ill_file_name[50] = "";
	strcat_s(ill_file_name, data_path);
	strcat_s(ill_file_name, diseasesList[iDisease]);
	strcat_s(ill_file_name, ".txt");

	// Reading healthy and ill people
	ObjFeatureMatrix healthyPeople(healthy_file_name, Healthy);
	ObjFeatureMatrix illPeople(ill_file_name, Ill);

	// CHANGE SIGNS SECTION
	doChangeSigns(healthyPeople, illPeople, 0.1, false);	
		
	//ofstream out("output.txt");
	
	// Greedy PFS
	//learnHealthyPeople.disableAllObjects();
	healthyPeople.disableAllFeatures();
	//learnIllPeople.disableAllObjects();
	illPeople.disableAllFeatures();
	//learnHealthyPeople.setObjectState(0, stENABLED);
	//learnHealthyPeople.setFeatureState(0, stENABLED);
	//learnIllPeople.setObjectState(0, stENABLED);
	//learnIllPeople.setFeatureState(0, stENABLED);

	greedyPFS greedyPFSInstance(healthyPeople, illPeople);
	greedyPFSInstance.runGreedyPFS(2);
	greedyPFSInstance.printReport(out);
	
	
	//system("pause");
	return 0;
}
*/
/**********************************************************************************
	* File: person.h
	* Description: class person is implemented
	* Created: 20.09.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

	Fields:
	- int _label -- Ill or Health
	- double features[] -- array containing features (3-gramm frequencies) 
	Methods:
	- readit -- reads personal information
		+ *in -- pointer to ifstream object from where we read
		+ rType -- Type of file to read
			- 0 -- file containes only vector of features
			- rTypeWithLabel -- file containes class label followed by vector of features
		+ sLabel -- label to set (can be ommited if rType = 0)

***********************************************************************************/

#pragma once
#include <iostream>
#include <fstream>
#include <vector>

// number of features
#define ngramm_vector_size 216
// length of the codogramm
#define codogramm_length 600
// label for ill people
#define Ill 1
// label for healthy people
#define Healthy -1

// Reading versions:
// first value is label, all next values are features
#define rTypeLabelFirst 1
// no label, all values are features
#define rTypeNoLabel 0
// all values except the last one are features, last one is label
#define rTypeLabelLast 2

using namespace std;

class person
{
private:
	// label (Healthy or Ill -- see in definitions.h)
	int _label; 
	// array containing features (3-gramm frequencies) 
	double features[ngramm_vector_size];
public:
	int getLabel();
	void setLabel(int sLabel);

	// Read the instance from file '*in'
	// rType -- Type of file to read (see in headline)
	// returns false if failed to read
	bool readit(FILE* in, int rType, int sLabel = 0);

	// access the feature number index
	double& operator[](int index);
};


#pragma once
#include <fstream>
#include <vector>

#include "ObjFeatureMatrix.h"
//#include "person.h"
#include "Algorithm.h"

using namespace std;

class crossValidation
{
private:
	vector< int > posIll, posHealthy;
	int nLearnHealthy, nLearnIll, nTestHealthy, nTestIll;

	void fillPos(vector< int > &pos, int nPeople);
public:
	double AUC;
	double learnAUC;
	double err;
	double learnErr;

	void doCrossValidation(Algorithm& alg, ObjFeatureMatrix& healthyPeople, ObjFeatureMatrix& illPeople, int nFolds);

	void report(ofstream* out);
};


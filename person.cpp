#include "person.h"

int person::getLabel(){
	return _label;
}

void person::setLabel(int sLabel){
	_label = sLabel;
}

bool person::readit(FILE* in, int rType, int sLabel)
{
	_label = sLabel;

	double temp = 0;
	//if end of file reached
	if (fscanf_s(in, "%lf", &temp) == EOF)
		return false;

	switch (rType)
	{
	//file containes class label followed by vector of features
	case rTypeLabelFirst:
		_label = (int) temp;
		for (int i = 0; i < ngramm_vector_size; i++)
			fscanf_s(in, "%lf", &features[i]);
		break;
	//file containes only vector of features
	case rTypeNoLabel:
		features[0] = temp;
		for (int i = 1; i < ngramm_vector_size; i++)
			fscanf_s(in, "%lf", &features[i]);
		break;
	case rTypeLabelLast:
		features[0] = temp;
		for (int i = 1; i < ngramm_vector_size; i++)
			fscanf_s(in, "%lf", &features[i]);
		fscanf_s(in, "%d", &_label);
		break;
	}
	return true;
}

double& person::operator[](int index){
	if (index >= 0 && index < ngramm_vector_size)
		return features[index];		
	else
		throw 2;
}
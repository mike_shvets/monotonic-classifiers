/**********************************************************************************
	* File: AUC.h
	* Description: given two vectors, real labels and predictions, counts AUC (area under ROC curve) value
	* Created: 04.10.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

#pragma once

#include <vector>
#include <algorithm>

//#include "person.h"
#include "ObjFeatureMatrix.h"

using namespace std;

/*
	makeVector of pairs: makes vector of pairs (prediction, real label) given vector of predictions and reference to vector of people
		+ for AUC -- vector of pairs, in which to write
		+ people -- array of people (from which we get real labels)
		+ predict -- vector of predictions
*/
int makeVectorOfPairs(vector< pair< double, int > >& forAUC, ObjFeatureMatrix& people, double* predict);

double countAUC(vector< pair< double, int > > a);

#include "ObjFeatureMatrix.h"

void ObjFeatureMatrix::activateAllObjects(void)
{
	nActiveObjects = _size;
	objectState.resize(_size);
	for (int i = 0; i < _size; i++)
		objectState[i] = stENABLED;
}

void ObjFeatureMatrix::activateAllFeatures(void)
{
	nActiveFeatures = ngramm_vector_size;
	if (featureState != NULL)
		delete[] featureState;
	featureState = new bool [ngramm_vector_size];
	for (int k = 0; k < ngramm_vector_size; k++)
		featureState[k] = stENABLED;
}

void ObjFeatureMatrix::disableAllObjects(void)
{
	nActiveObjects = 0;
	objectState.resize(_size);
	for (int i = 0; i < _size; i++)
		objectState[i] = stDISABLED;
}

void ObjFeatureMatrix::disableAllFeatures(void)
{
	nActiveFeatures = 0;
	if (featureState != NULL)
		delete[] featureState;
	featureState = new bool [ngramm_vector_size];
	for (int k = 0; k < ngramm_vector_size; k++)
		featureState[k] = stDISABLED;
}

ObjFeatureMatrix::ObjFeatureMatrix(void)
{
	_size = 0;
	featureState = NULL;
	activateAllFeatures();
	activateAllObjects();
}

ObjFeatureMatrix::ObjFeatureMatrix(char fileName[], int label)
{
	featureState = NULL;
	FILE* input;
	fopen_s(&input, fileName, "r");

	fscanf_s(input, "%d", &_size);

	people.reserve(_size);
	for (int i = 0; i < _size; i++)
	{
		person temp;
		//push to people only if temp was correctly read
		if (temp.readit(input, rTypeNoLabel, label))
			people.push_back(temp);
	}

	fclose(input);

	if (_size != people.size())
		throw "Error while reading";

	activateAllFeatures();
	activateAllObjects();
}

ObjFeatureMatrix::~ObjFeatureMatrix(void)
{
	delete[] featureState;
}

int ObjFeatureMatrix::size(void)
{
	return _size;
}

void ObjFeatureMatrix::clear()
{
	people.clear();
	_size = 0;
	objectState.clear();
	nActiveObjects = 0;

	activateAllFeatures();
}

void ObjFeatureMatrix::copyFeaturesStates(ObjFeatureMatrix &M)
{
	for (int k = 0; k < ngramm_vector_size; k++)
		this->setFeatureState(k, M.getFeatureState(k));
}

void ObjFeatureMatrix::pushBack(person oneMore, bool state)
{
	_size++;
	people.push_back(oneMore);
	if (state == stENABLED)
		nActiveObjects++;
	objectState.push_back(state);
}

person& ObjFeatureMatrix::operator[](int index)
{
	if (index < _size && index >= 0)
		return people[index];
	else
		throw 1;
}


int ObjFeatureMatrix::numberOfActiveObjects( void )
{
	return nActiveObjects;
}

bool ObjFeatureMatrix::getObjectState(int iObj)
{
	if (iObj < _size && iObj >= 0)
		return objectState[iObj];
	else
		return false;
}

void ObjFeatureMatrix::setObjectState(int iObj, bool state)
{
	if (iObj < _size && iObj >= 0)
	{
		if (objectState[iObj] != state)
		{
			if (state == stDISABLED)
				nActiveObjects--;
			else
				nActiveObjects++;
		}
		objectState[iObj] = state;
	}
	else
		throw 1;
}

int ObjFeatureMatrix::numberOfActiveFeatures( void )
{
	return nActiveFeatures;
}

bool ObjFeatureMatrix::getFeatureState(int iFeature)
{
	if (iFeature < ngramm_vector_size && iFeature >= 0)
		return featureState[iFeature];
	else
		return false;
}

void ObjFeatureMatrix::setFeatureState(int iFeature, bool state)
{
	if (iFeature < ngramm_vector_size && iFeature >= 0)
	{
		if (featureState[iFeature] != state)
		{
			if (state == stDISABLED)
				nActiveFeatures--;
			else
				nActiveFeatures++;
		}
		featureState[iFeature] = state;
	}
	else
		throw 1;
}

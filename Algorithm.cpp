/**********************************************************************************
	* File: Algorithm.cpp
	* Description: implementation file
	* Created: 01.10.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

#include "Algorithm.h"

Algorithm::Algorithm(void)
{
	ans = NULL;
}

Algorithm::~Algorithm(void)
{
}

void Algorithm::learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople)
{
}

void Algorithm::test(ObjFeatureMatrix &testPeople)
{
}

void Algorithm::report(ofstream *out)
{
}
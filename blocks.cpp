#include "blocks.h"

vector< bool > doChangeSigns(ObjFeatureMatrix& healthyPeople, ObjFeatureMatrix& illPeople, double treshold, bool printReport)
{
	vector< bool > res;

	FILE* outputReport = NULL;
	int changeSignsReportFlag = 0;
	
	if (printReport)
	{
		fopen_s(&outputReport, "reports\\Change_signs.txt", "w");
		fprintf_s(outputReport, "Change signs section report\n\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	vector<int> sumHlth, sumIll;
	for (int j = 0; j < ngramm_vector_size; j++)
	{
		sumHlth.push_back(0);
		sumIll.push_back(0);

		for (int i = 0; i < healthyPeople.size(); i++)
			sumHlth[j] += healthyPeople[i][j];

		for (int i = 0; i < illPeople.size(); i++)	
			sumIll[j] += illPeople[i][j];
	}

	int nChanged = 0, nDisabled = 0;
	for (int j = 0; j < ngramm_vector_size; j++)
	{
		if ( abs( (sumHlth[j] + .0)/healthyPeople.size() - (sumIll[j] + .0)/illPeople.size() ) < treshold )
		{
			healthyPeople.setFeatureState(j, stDISABLED);
			illPeople.setFeatureState(j, stDISABLED);
			nDisabled++;
		}
		
		// compare average values for healthy and ill people
		// if average value for healthy people is more, than change signs
		if (sumHlth[j]*illPeople.size() > sumIll[j]*healthyPeople.size())
		{
			for (int i = 0; i < healthyPeople.size(); i++)
				healthyPeople[i][j] = -healthyPeople[i][j] - 0.5;
			for (int i = 0; i < illPeople.size(); i++)
				illPeople[i][j] = -illPeople[i][j] - 0.5;
			nChanged++;

			res.push_back(true);
		}
		else
			res.push_back(false);
	}

	if (printReport)
	{
		fprintf_s(outputReport, "%d features changed sign\n%d features were disabled\n", nChanged, nDisabled);
		fprintf_s(outputReport, "Mean values of healthy and ill people:\n");
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			fprintf_s(outputReport, "Feature %d: %lf   %lf", k, (sumHlth[k] + .0)/healthyPeople.size(),  (sumIll[k] + .0)/illPeople.size());
			if (!healthyPeople.getFeatureState(k))
				fprintf_s(outputReport, "   (DISABLED)\n");
			else
				fprintf_s(outputReport, "\n");
		}
	}

	if (printReport)
	{
		fprintf_s(outputReport, "\nOverall progress: ");
		if (changeSignsReportFlag == 0)
			fprintf_s(outputReport, "SUCCESS\n");
		else
		if (changeSignsReportFlag == 1)
			fprintf_s(outputReport, "FAIL\n");
		else
			fprintf_s(outputReport, "WARNINGS\n");

		fclose(outputReport);
	}

	return res;
}

// Return whether the double value of elem1 is greater than double value of elem2
bool comp ( pair< int, double > elem1, pair< int, double > elem2 )
{
	return elem1.second > elem2.second;
}

int binar(double t)
{
	double eps = 1e-8;
	if (t >= tTreshold - eps)
		return 1;
	else
		return 0;
}

vector< pair< int, double > > getWeights(ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople, int mGetWeights, bool printWeightsVector)
{
	vector< double > weights(ngramm_vector_size);
	if (mGetWeights == mGetWeightsBayes)
	{
		// make instance of naive bayesian algorithm
		NBayes naiveFeatureSelection;
		naiveFeatureSelection.learn(healthyPeople, illPeople);
		for (int k = 0; k < ngramm_vector_size; k++)
			weights[k] = abs(naiveFeatureSelection.weights[k]);
	}
	// mean occurrence of 3-gramm in ILL people's kardiogramm
	else if (mGetWeights == mGetWeightsMediumFreq)
	{
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			weights[k] = 0;
			for (int j = 0; j < illPeople.size(); j++)
				weights[k] += illPeople[j][k];
			weights[k] /= illPeople.size();
		}
	}
	// difference between mean occurrence of 3-gramm in ILL people's and in HEALTHY people's kardiogramms
	else if (mGetWeights == mGetWeightsMediumFreqDiff)
	{
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			weights[k] = 0;
			for (int j = 0; j < illPeople.size(); j++)
				weights[k] += illPeople[j][k];
			weights[k] /= illPeople.size();

			double temp = 0;
			for (int i = 0; i < healthyPeople.size(); i++)
				temp += healthyPeople[i][k];
			weights[k] -= temp / healthyPeople.size();
		}
	}
	else if (mGetWeights == mGetWeightsMediumFreqDiffAbs)
	{
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			weights[k] = 0;
			for (int j = 0; j < illPeople.size(); j++)
				weights[k] += illPeople[j][k];
			weights[k] /= illPeople.size();

			double temp = 0;
			for (int i = 0; i < healthyPeople.size(); i++)
				temp += healthyPeople[i][k];
			weights[k] -= temp / healthyPeople.size();
			weights[k] = abs(weights[k]);
		}
	}
	else if (mGetWeights == mGetWeightsOccurrence)
	{
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			weights[k] = 0;
			for (int j = 0; j < illPeople.size(); j++)
				weights[k] += binar(illPeople[j][k]);
			weights[k] /= illPeople.size();
		}
	}
	else if (mGetWeights == mGetWeightsOccurrenceDiff)
	{
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			weights[k] = 0;
			for (int j = 0; j < illPeople.size(); j++)
				weights[k] += binar(illPeople[j][k]);
			weights[k] /= illPeople.size();

			double temp = 0;
			for (int i = 0; i < healthyPeople.size(); i++)
				temp += binar(healthyPeople[i][k]);
			weights[k] -= temp / healthyPeople.size();
		}
	}
	else if (mGetWeights == mGetWeightsOccurrenceDiffAbs)
	{
		for (int k = 0; k < ngramm_vector_size; k++)
		{
			weights[k] = 0;
			for (int j = 0; j < illPeople.size(); j++)
				weights[k] += binar(illPeople[j][k]);
			weights[k] /= illPeople.size();

			double temp = 0;
			for (int i = 0; i < healthyPeople.size(); i++)
				temp += binar(healthyPeople[i][k]);
			weights[k] -= temp / healthyPeople.size();
			weights[k] = abs(weights[k]);
		}
	}

	vector< pair< int, double > > res;
	res.reserve(ngramm_vector_size);
	for (int k = 0; k < ngramm_vector_size; k++)
		res.push_back(pair<int, double>(k, weights[k]));

	sort(res.begin(), res.end(), comp);

	if (printWeightsVector)
	{
		ofstream outNB;
		if (mGetWeights == mGetWeightsBayes)
			outNB.open("reports\\NaiveBayesWeightsVector.txt");
		else if (mGetWeights == mGetWeightsMediumFreq)
			outNB.open("reports\\MediumFreqWeightsVector.txt");
		else if (mGetWeights == mGetWeightsMediumFreqDiff)
			outNB.open("reports\\MediumFreqDiffWeightsVector.txt");
		else if (mGetWeights == mGetWeightsOccurrence)
			outNB.open("reports\\OccurrenceWeightsVector.txt");

		outNB << "Sorted vector:" << endl;
		for (int k = 0; k < ngramm_vector_size; k++)
			outNB << "Feature " << res[k].first << ": " << res[k].second << endl;

		outNB << endl << endl << endl << "Not sorted vector:" << endl;
		for (int k = 0; k < ngramm_vector_size; k++)
			outNB << "Feature " << k << ": " << weights[k] << endl;

		outNB.close();
	}

	return res;
}

// leaves only nFeatures enabled, if nFeatures < amount of active features
// else does nothing
// Parameters:
// nFeatures -- number of features
// weight -- vector of pairs (iFeature, iFeature_weight)
void doPreliminaryFeatureSelection(int nFeatures, vector< pair< int, double > >& weight,
						ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople)
{
	sort(weight.begin(), weight.end(), comp);

	// stores the amount of currently active features
	int count = 0;
	int k = 0;
	// skip enabled nFeatures with maximal weight  
	while (count < nFeatures && k < ngramm_vector_size)
	{
		if (healthyPeople.getFeatureState(weight[k].first) == stENABLED)
			count++;
		k++;
	}

	while (k < ngramm_vector_size)
	{
		healthyPeople.setFeatureState(weight[k].first, stDISABLED);
		illPeople.setFeatureState(weight[k].first, stDISABLED);
		k++;
	}
}

/*
void printClassificationReport(crossValidation& CV)
{
	int classificationReportFlag = 0;
	ofstream classificationReportOut("unit_tests\\Classification.txt");
	classificationReportOut << "Classification section unit test report (monotonic classifier)" << endl << endl;

	CV.unitTestReport(&classificationReportOut, classificationReportFlag);

	classificationReportOut << "Overall progress: ";
	if (classificationReportFlag != 0)
		classificationReportOut << "FAIL" << endl;
	else
		classificationReportOut << "SUCCESS" << endl;

	classificationReportOut.close();
}

*/

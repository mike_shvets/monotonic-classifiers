#include "MonotonicNN.h"

#define eps 10e-8

#define tUpper 1
#define tLower -1

double MonotonicNN::distToShadow(person& from, person& to, int type)
{
	double res = 0;

	for (int k = 0; k < ngramm_vector_size; k++)
	if (healthyPrototypes.getFeatureState(k) == stENABLED)
	{
		double s = from[k] - to[k];
		if (type == tLower && s > 0)
			res += s;
		if (type == tUpper && s < 0)
			res -= s;
	}

	return res;
}

person* MonotonicNN::findNNByShadow(person &current, ObjFeatureMatrix &prototypes, int type)
{
	double minDist = 10e8;
	person* res = NULL;

	for (int i = 0; i < prototypes.size(); i++)
	if (prototypes.getObjectState(i) == stENABLED)
	{
		double curDist = distToShadow(current, prototypes[i], type);
		if (curDist < minDist)
		{
			minDist = curDist;
			res = &prototypes[i];
		}
	}
		return res;
}

bool MonotonicNN::isInShadowOfB(person &a, person &b)
{
	if (b.getLabel() == Healthy)
	{
		if(aDominatesB(b, a))
			return true;
		else
			return false;
	}
	else
	if (aDominatesB(a, b))
		return true;
	else
		return false;	
}

void MonotonicNN::prototypeSelection()
{
	for (int i = 0; i < healthyPrototypes.size() - 1; i++)
	if (healthyPrototypes.getObjectState(i) == stENABLED)
	{
		for (int j = i+1; j < healthyPrototypes.size(); j++)
		if (healthyPrototypes.getObjectState(j) == stENABLED)
		{
			if (isInShadowOfB(healthyPrototypes[i], healthyPrototypes[j]))
				healthyPrototypes.setObjectState(i, stDISABLED);
			else
			if (isInShadowOfB(healthyPrototypes[j], healthyPrototypes[i]))
				healthyPrototypes.setObjectState(j, stDISABLED);
		}
	}

	for (int i = 0; i < illPrototypes.size() - 1; i++)
	if (illPrototypes.getObjectState(i) == stENABLED)
	{
		for (int j = i+1; j < illPrototypes.size(); j++)
		if (illPrototypes.getObjectState(j) == stENABLED)
		{
			if (isInShadowOfB(illPrototypes[i], illPrototypes[j]))
				illPrototypes.setObjectState(i, stDISABLED);
			else
			if (isInShadowOfB(illPrototypes[j], illPrototypes[i]))
				illPrototypes.setObjectState(j, stDISABLED);
		}
	}
}

bool MonotonicNN::aDominatesB(person &a, person &b)
{
	bool res = true;
	for (int k = 0; k < ngramm_vector_size; k++)
		if (healthyPrototypes.getFeatureState(k) == stENABLED)
			if (a[k] < b[k] - eps)
			{
				res = false;
				break;
			}
	return res;
}

/*
void MonotonicNN::greedyMonotonization()
{
	nPairs = healthyPrototypes.size() * illPrototypes.size();
	nMonotonic = 0;
	nViolating = 0;

	vector< int > nViolHealthy(healthyPrototypes.size());
	vector< int > nViolIll(illPrototypes.size());
	vector< set< int > > healthyViolList(healthyPrototypes.size());
	vector< set< int > > illViolList(illPrototypes.size());

	fill(nViolHealthy.begin(), nViolHealthy.end(), 0);
	fill(nViolIll.begin(), nViolIll.end(), 0);

	for (int i = 0; i < healthyPrototypes.size(); i++)
		for (int j = 0; j < illPrototypes.size(); j++)
			if (aDominatesB(healthyPrototypes[i], illPrototypes[j]))
			{
				nViolHealthy[i]++;
				healthyViolList[i].insert(j);
				nViolIll[j]++;
				illViolList[j].insert(i);

				nViolating++;
			}
			else
			if (aDominatesB(illPrototypes[j], healthyPrototypes[i]))
				nMonotonic++;

	int maxViol = 1;
	while (maxViol != 0)
	{
		int label = Healthy;
		int ind = 0;
		maxViol = 0;

		if (healthyPrototypes.numberOfActiveObjects() > 1)
		for (int i = 0; i < healthyPrototypes.size(); i++)
		if (nViolHealthy[i] > maxViol)
		{
			maxViol = nViolHealthy[i];
			ind = i;
		}

		if (illPrototypes.numberOfActiveObjects() > 1)
		for (int j = 0; j < illPrototypes.size(); j++)
		if (nViolIll[j] > maxViol)
		{
			maxViol = nViolIll[j];
			ind = j;
			label = Ill;
		}

		if (maxViol == 0)
			break;

		nThrownOut++;
		if (label == Healthy)
		{
			healthyPrototypes.setObjectState(ind, stDISABLED);
			for (set< int >::iterator it = healthyViolList[ind].begin();
				it != healthyViolList[ind].end(); it++)
			{
				nViolIll[*it]--;
				illViolList[*it].erase(ind);
			}
			nViolHealthy[ind] = 0;
			healthyViolList[ind].clear();
		}
		else
		{
			illPrototypes.setObjectState(ind, stDISABLED);
			for (set< int >::iterator it = illViolList[ind].begin();
				it != illViolList[ind].end(); it++)
			{
				nViolHealthy[*it]--;
				healthyViolList[*it].erase(ind);
			}
			nViolIll[ind] = 0;
			illViolList[ind].clear();
		}
	}


	for (int i =0; i < healthyPrototypes.size(); i++)
		if(healthyPrototypes.getObjectState(i) == stENABLED)
		for (int j = 0; j < illPrototypes.size(); j++)
			if (illPrototypes.getObjectState(j) == stENABLED)
			if (aDominatesB(healthyPrototypes[i], illPrototypes[j]))
				throw "Error in monotonization";
}
*/

void MonotonicNN::greedyMonotonization2()
{
	// total number of pairs
	nPairs = healthyPrototypes.size() * illPrototypes.size();
	nMonotonic = 0;
	nViolating = 0;

	// nViolHealthy[i] is a number of violating pairs formed with i-th health
	vector< int > nViolHealthy(healthyPrototypes.size());
	vector< int > nViolIll(illPrototypes.size());
	// healthyViolList[i] is a set of numbers of ill people, who form violating pairs with i-th health 
	vector< set< int > > healthyViolList(healthyPrototypes.size());
	vector< set< int > > illViolList(illPrototypes.size());

	fill(nViolHealthy.begin(), nViolHealthy.end(), 0);
	fill(nViolIll.begin(), nViolIll.end(), 0);

	for (int i = 0; i < healthyPrototypes.size(); i++)
		for (int j = 0; j < illPrototypes.size(); j++)
			// if pair (i, j) is violating
			if (aDominatesB(healthyPrototypes[i], illPrototypes[j]))
			{
				nViolHealthy[i]++;
				healthyViolList[i].insert(j);
				nViolIll[j]++;
				illViolList[j].insert(i);

				nViolating++;
			}
			else
			if (aDominatesB(illPrototypes[j], healthyPrototypes[i]))
				nMonotonic++;

	double max = 0.001;
	while (max > 0)
	{
		int label = Healthy;
		int ind = 0;
		max = -1;

		if (healthyPrototypes.numberOfActiveObjects() > 1)
		for (int i = 0; i < healthyPrototypes.size(); i++)
		if ((nViolHealthy[i] + .0) / illPrototypes.numberOfActiveObjects() > max)
		{
			max = (nViolHealthy[i] + .0) / illPrototypes.numberOfActiveObjects();
			ind = i;
		}

		if (illPrototypes.numberOfActiveObjects() > 1)
		for (int j = 0; j < illPrototypes.size(); j++)
		if ((nViolIll[j] + .0) / healthyPrototypes.numberOfActiveObjects() > max)
		{
			max = (nViolIll[j] + .0) / healthyPrototypes.numberOfActiveObjects();
			ind = j;
			label = Ill;
		}

		if (max < 0)
			break;

		nThrownOut++;
		if (label == Healthy)
		{
			healthyPrototypes.setObjectState(ind, stDISABLED);
			for (set< int >::iterator it = healthyViolList[ind].begin();
				it != healthyViolList[ind].end(); it++)
			{
				nViolIll[*it]--;
				illViolList[*it].erase(ind);
			}
			nViolHealthy[ind] = 0;
			healthyViolList[ind].clear();
		}
		else
		{
			illPrototypes.setObjectState(ind, stDISABLED);
			for (set< int >::iterator it = illViolList[ind].begin();
				it != illViolList[ind].end(); it++)
			{
				nViolHealthy[*it]--;
				healthyViolList[*it].erase(ind);
			}
			nViolIll[ind] = 0;
			illViolList[ind].clear();
		}
	}


	for (int i =0; i < healthyPrototypes.size(); i++)
		if(healthyPrototypes.getObjectState(i) == stENABLED)
		for (int j = 0; j < illPrototypes.size(); j++)
			if (illPrototypes.getObjectState(j) == stENABLED)
			if (aDominatesB(healthyPrototypes[i], illPrototypes[j]))
				throw "Error in monotonization";
}

void MonotonicNN::setAlpha(double a)
{
	alpha = a;
}

void MonotonicNN::greedyMonotonizationAlpha(double alpha)
{
	// total number of pairs
	nPairs = healthyPrototypes.size() * illPrototypes.size();
	nMonotonic = 0;
	nViolating = 0;
	nThrownOut = 0;

	// nViolHealthy[i] is a number of violating pairs formed with i-th health
	vector< int > nViolHealthy(healthyPrototypes.size());
	vector< int > nViolIll(illPrototypes.size());
	// healthyViolList[i] is a set of numbers of ill people, who form violating pairs with i-th health 
	vector< set< int > > healthyViolList(healthyPrototypes.size());
	vector< set< int > > illViolList(illPrototypes.size());

	// number of ill and healthy people forming violating pairs
	int nIllV = 0, nHealthyV = 0;

	// initialization
	fill(nViolHealthy.begin(), nViolHealthy.end(), 0);
	fill(nViolIll.begin(), nViolIll.end(), 0);

	for (int i = 0; i < healthyPrototypes.size(); i++)
		for (int j = 0; j < illPrototypes.size(); j++)
			// if pair (i, j) is violating
			if (aDominatesB(healthyPrototypes[i], illPrototypes[j]))
			{
				if (nViolHealthy[i] == 0)
					nHealthyV++;
				nViolHealthy[i]++;
				healthyViolList[i].insert(j);
				if (nViolIll[j] == 0)
					nIllV++;
				nViolIll[j]++;
				illViolList[j].insert(i);

				nViolating++;
			}
			else
			if (aDominatesB(illPrototypes[j], healthyPrototypes[i]))
				nMonotonic++;


	// disabling alpha percent of ill people forming violating pairs
	bool flag = (nHealthyV == healthyPrototypes.numberOfActiveObjects());
	for (int t = 0; t < alpha*nIllV || flag; t++)
	{
		nThrownOut++;
		int maxViol = 0;
		int ind = -1;
		for (int j = 0; j < illPrototypes.size(); j++)
			if (nViolIll[j] > maxViol)
			{
				maxViol = nViolIll[j];
				ind = j;
			}
		illPrototypes.setObjectState(ind, stDISABLED);
		for (set< int >::iterator it = illViolList[ind].begin(); it != illViolList[ind].end(); it++)
		{
			nViolHealthy[*it]--;
			if (nViolHealthy[*it] == 0)
				flag = false;
			healthyViolList[*it].erase(ind);
		}
		nViolIll[ind] = 0;
		illViolList[ind].clear();
	}

	for (int i = 0; i < healthyPrototypes.size(); i++)
		if (nViolHealthy[i] > 0)
		{
			nThrownOut++;
			healthyPrototypes.setObjectState(i, stDISABLED);
			for (set< int >::iterator it = healthyViolList[i].begin();
				it != healthyViolList[i].end(); it++)
			{
				nViolIll[*it]--;
				illViolList[*it].erase(i);
			}
			nViolHealthy[i] = 0;
			healthyViolList[i].clear();
		}

	for (int i =0; i < healthyPrototypes.size(); i++)
		if(healthyPrototypes.getObjectState(i) == stENABLED)
		for (int j = 0; j < illPrototypes.size(); j++)
			if (illPrototypes.getObjectState(j) == stENABLED)
			if (aDominatesB(healthyPrototypes[i], illPrototypes[j]))
				throw "Error in monotonization";
}


void MonotonicNN::resetAll()
{
	nPairs = nMonotonic = nViolating = 0;
	nThrownOut = 0;

	healthyPrototypes.clear();
	illPrototypes.clear();

	if (ans != NULL)
	{
		delete[] ans;
		ans = NULL;
	}
};

void MonotonicNN::copyLearningSample(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople)
{
	healthyPrototypes.copyFeaturesStates(learnHealthyPeople);
	illPrototypes.copyFeaturesStates(learnIllPeople);

	for (int i = 0; i < learnHealthyPeople.size(); i++)
		if (learnHealthyPeople.getObjectState(i))
			healthyPrototypes.pushBack(learnHealthyPeople[i], learnHealthyPeople.getObjectState(i));
	for (int j = 0; j < learnIllPeople.size(); j++)
		if (learnIllPeople.getObjectState(j))
			illPrototypes.pushBack(learnIllPeople[j], learnIllPeople.getObjectState(j));
}

MonotonicNN::MonotonicNN()
{
	ans = NULL;
	alpha = 0.9;
}

void MonotonicNN::learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople)
{
	resetAll();
	
	copyLearningSample(learnHealthyPeople, learnIllPeople);
	
	//greedyMonotonization2();
	greedyMonotonizationAlpha(alpha);

	prototypeSelection();
}

double MonotonicNN::phi(person &a)
{
	double res = 0;
	for (int k = 0; k < ngramm_vector_size; k++)
		if (healthyPrototypes.getFeatureState(k) == stENABLED)
			res += a[k];
	return res - 2*healthyPrototypes.numberOfActiveFeatures();
}

double MonotonicNN::H(person &a)
{
	double phi_a = phi(a);
	
	if (phi_a > Ill)
	{
		double distToHealthyLowerShadow = 
			distToShadow(a, *findNNByShadow(a, healthyPrototypes, tLower), tLower);
		double distToIllLowerShadow = 
			distToShadow(a, *findNNByShadow(a, illPrototypes, tLower), tLower);

		if (distToHealthyLowerShadow < distToIllLowerShadow)
			return distToHealthyLowerShadow;
		else
			return distToIllLowerShadow;
	}
	else
	if (phi_a < Healthy)
	{
		double distToHealthyUpperShadow = 
			distToShadow(a, *findNNByShadow(a, healthyPrototypes, tUpper), tUpper); 
		double distToIllUpperShadow = 
			distToShadow(a, *findNNByShadow(a, illPrototypes, tUpper), tUpper); 

		if (distToHealthyUpperShadow < distToIllUpperShadow)
			return distToHealthyUpperShadow;
		else
			return distToIllUpperShadow;
	}
	else
		return 0;
}

void MonotonicNN::test(ObjFeatureMatrix &testPeople)
{
	if (ans != NULL)
		delete[] ans;
	ans = new double[testPeople.size()];

	for (int i = 0; i < testPeople.size(); i++)
	{
		person* nearestHealthy = findNNByShadow(testPeople[i], healthyPrototypes, tLower);
		person* nearestIll = findNNByShadow(testPeople[i], illPrototypes, tUpper);

		double distToNearestHealthy = distToShadow(testPeople[i], *nearestHealthy, tLower);
		double distToNearestIll = distToShadow(testPeople[i], *nearestIll, tUpper);
		
		/*
		if (distToNearestHealthy < distToNearestIll)
			ans[i] = Healthy;
		else
			ans[i] = Ill;
		*/

		
		if (distToNearestHealthy < eps)
			ans[i] = Healthy;
		else
		if (distToNearestIll < eps)
			ans[i] = Ill;
		else
			ans[i] = (distToNearestHealthy - distToNearestIll)/(distToNearestHealthy + distToNearestIll);
			
		/*
		double C = 1000000000;
		person &a = testPeople[i];
		ans[i] = (C * ans[i] + H(a)*phi(a)) / (C + H(a));
		*/
	}
}	

void MonotonicNN::report(ofstream *out)
	{
		*out << "***Monotonic nearest neighbour section***" << endl << endl;
		*out << "Total number of pairs in learning sample: " << nPairs << endl <<
		"including " << nMonotonic << " monotonic" << endl <<
		"and " << nViolating << " violating" << endl << endl;
		*out << nThrownOut <<" objects were thrown out" << endl << endl;

		*out << "Number of prototypes selected -- " << healthyPrototypes.numberOfActiveObjects() 
			<< " healthy and "
			<< illPrototypes.numberOfActiveObjects() << " ill." << endl
			<< endl << endl;
		*out << "---------------------------------------------------------------------------" << endl << endl;
	}



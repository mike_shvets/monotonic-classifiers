/**********************************************************************************
	* File: MonotonicNN.h
	* Description: Monotonic nearest neighbour: given monotonic training sample
	builds model of monotonic nearest neighbour 
	* Created: 23.09.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

#pragma once
#include <vector>
#include <algorithm>

#include "ObjFeatureMatrix.h"
#include "Algorithm.h"

using namespace std;

class NN: public Algorithm
{
private:
	ObjFeatureMatrix healthyPrototypes;
	ObjFeatureMatrix illPrototypes;
	
	// distToShadow -- distance from person 'from' to person 'to'
	double dist(person& from, person& to);

	// findNNByShadow -- given a person 'current' finds nearest (by shadow) neighbour from the array of people 'people'
	person* findNNByShadow(person &current, ObjFeatureMatrix &prototypes);
	
	void copyLearningSample(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople);

	void resetAll();

public:
	//double *ans;

	NN();

	virtual void learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople);

	virtual void test(ObjFeatureMatrix &testPeople);
};


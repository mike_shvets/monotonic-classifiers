#include "AUC.h"

int makeVectorOfPairs(vector< pair< double, int > >& forAUC, ObjFeatureMatrix& people, double* predict)
{
	int err = 0;
	forAUC.resize(people.size());
	for (int i = 0; i < people.size(); i++)
	{
		forAUC.at(i) = make_pair(predict[i], people[i].getLabel());
		// If real and predicted labels have different signs
		if (people[i].getLabel() * predict[i] < 0)
			err++;
	}

	return err;
}

bool myComparator (pair< double, int > u, pair< double, int > v) { return (u.first > v.first); }

double countAUC(vector< pair< double, int > > a)
{
	double nIll = 0, nHealth = 0;
	for (int i = 0; i < a.size(); i++)
		if (a[i].second == Ill) nIll++;
		else nHealth++;

	random_shuffle(a.begin(), a.end());
	sort(a.begin(), a.end(), myComparator);
	
	double FPR = 0, TPR = 0;
	double AUC = 0;
	for (int i = 0; i < a.size(); i++)
		if (a[i].second == Healthy)
		{
			FPR += 1/nHealth;
			AUC += TPR/nHealth;
		}
		else
		{
			TPR += 1/nIll;
		}

	return AUC;
}

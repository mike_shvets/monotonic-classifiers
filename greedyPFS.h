/**********************************************************************************
	* File: greedyPFS.h
	* Description: greedy prototype and feature selection
	* Created: 25.12.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

	
***********************************************************************************/

#pragma once
#define _SCL_SECURE_NO_WARNINGS
#include <vector>
#include <list>
#include <map>

#include "ObjFeatureMatrix.h"

#define mAdd 1
#define mDel -1
#define mNothing 0

class greedyPFS
{
private:
	// pointer to matrix of healthy people
	ObjFeatureMatrix *pHealthyPeople;
	// pointer to matrix of ill people
	ObjFeatureMatrix *pIllPeople;

	bool *act0;
	bool *act1;
	bool *actF;

	int n0, n1, nF;
	int nAct0, nAct1, nActF;

	// T(i,j,k) = T-matrix[i][j][k]
	// T-matrix -- 3-dimensional matrix
	// T[i][j][k] = false, if k-th feature of i-th healthy person is bigger than
	//                   k-th feature of j-th ill person
	//            = true, else
	bool T(int i, int j, int k);

	// number of monotonic pairs
	int nMonotonic;
	// number of violating pairs
	int nViolating;

	// TODO:
	int nAlmostMonotonic;
	int nAlmostViolating;

	int nActivePairs;

	// 2-dimensional array
	// (i,j)-th element represents the monotonicity degree -- number of features in which the pair is monotonic
	int **matSum;

	// deletes (if param == -1) or adds (if param == 1) each element in the dimension dim and finds locally optimal solution;
	// returns a pair (int resNum, double max), where max is maximum value of the quality functions, 
	// which was reached by deleting element with number 
	// returns (-1, 0) if failed to make a step
	pair< int, double > greedyStep(int param, int dim);
	// makes one greedy del step
	// returns (-1, 0) if failed to make a step
	pair< int, double > greedyDelStep(int dim);
	// makes one greedy add step
	// returns (-1, 0) if failed to make a step
	pair< int, double > greedyAddStep(int dim);

	// updates nMonotonic and nViolating
	void countMonotonicAndViolating(void);

	// vector of pairs (dim, num)
	// element num in the dimension dim changed its state on the i-th step
	vector< pair< int, int >> report;
	// vector of pairs (nMonotonic, nViolating)
	vector< pair< int, int >> pairMonViol;
	vector< pair< int, int >> pairAlmostMonViol;
	// vector of actions
	// +1 if element was enabled
	// -1 if element was disabled
	vector< int > action;

	double curQF, globalBestQF;
	double prevQF;
	bool *act0Best, *act1Best, *actFBest;
	bool *act0GlobalBest, *act1GlobalBest, *actFGlobalBest;
	int iteration, iterationGlobalBest;

public:	
	greedyPFS( ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople );
	~greedyPFS();

	
	double qualityFunc(int funcNum = 0);

	// sets state of element num in dimension dim to state
	void setElementState(int dim, int num, bool state);

	// makes one step (add or del)
	// mode == mAdd if Add step is made
	//		== mDel if Del step i made
	// parameterD -- the allowed number of steps, during which quality functional can be decreasing
	void cycle(int mode0, int mode1, int modeF, int parameterD);

	// 
	void runGreedyPFS(int parameterD);

	// int signA == +1 if feature was added
	//          == -1 if feature was deleted 
	void changeMatSum(int fNum, int mode);

	void printReport(ofstream &out);
};



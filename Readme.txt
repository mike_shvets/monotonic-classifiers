Motivation:
The main goal of the project is to solve a problem of medical diagnostics of different types of diseases for patients by their electrocardiograms.
We research the application of monotone classifiers methods for the stated problem. 

Author:
Mikhail Shvets
mikhail.shvets@phystech.edu

Structure:
main.cpp

Project includes multiple modules.
All modules have header file *.h.
Some modules have *.cpp file.
Each file has a short description in the headline.
Each function or method has full description in *.h file before its implementation. 
In *.h file includes declarations and implementation of some (or all) functions and methods. 
If *.cpp is present, it includes implementations of some (or all) functions and methods, which were not implemented in header file.
Some files also include extra comments.
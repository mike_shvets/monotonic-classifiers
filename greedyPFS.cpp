#include "greedyPFS.h"

bool greedyPFS::T(int i, int j, int k)
{
	return (*pHealthyPeople)[i][k] <= (*pIllPeople)[j][k];
}

greedyPFS::greedyPFS( ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople )
{
	pHealthyPeople = &healthyPeople;
	pIllPeople = &illPeople;

	// T-matrix size initialization
	n0 = healthyPeople.size();
	n1 = illPeople.size();
	nF = ngramm_vector_size;

	// act0, act1, actF initialization
	act0 = new bool[ n0 ];
	for (int i = 0; i < n0; i++)
		act0[i] = healthyPeople.getObjectState(i);
	act1 = new bool[ n1 ];
	for (int j = 0; j < illPeople.size(); j++)
		act1[j] = illPeople.getObjectState(j);
	actF = new bool[ nF ];
	for (int k = 0; k < nF; k++)
		actF[k] = healthyPeople.getFeatureState(k);	

	// number of active elements
	nAct0 = healthyPeople.numberOfActiveObjects();
	nAct1 = illPeople.numberOfActiveObjects();
	nActF = healthyPeople.numberOfActiveFeatures();

	// matSum initialization
	matSum = new int*[ healthyPeople.size() ];
	for (int i = 0; i < healthyPeople.size(); i++)
	{
		matSum[i] = new int[ illPeople.size() ];
		for (int j = 0; j < illPeople.size(); j++)
		{
			matSum[i][j] = 0;
			for (int k = 0; k < ngramm_vector_size; k++)
				if (actF[k] == stENABLED)
				if (T(i,j,k)) matSum[i][j]++;
		}
	}

	globalBestQF = -10000;
}

// counts value of the quality function number funcNum
// funcNum:
// 0 -- (nMon - nViol)/(nMon + nViol)
// 1 -- nMon/(nViol+1)
double greedyPFS::qualityFunc(int funcNum)
{
	double res = 0;
	countMonotonicAndViolating();
	
	switch (funcNum)
	{
	case 0:
		res = (nMonotonic -  nViolating + .0)/(nMonotonic + nViolating + 1);
		break;
	case 1:
		// ratio
		res = (nMonotonic + .0) / (nViolating + 1);
		break;
	case 2:
		res = (nMonotonic * 2 + nAlmostMonotonic + .0) / (nViolating*2 + nAlmostViolating + 10);
		break;
	}
	
	return res;
}

void greedyPFS::setElementState(int dim, int num, bool state)
{
	switch (dim)
	{
		case 0:
			if (act0[num] != state){
				if (state == stENABLED) nAct0++;
				else nAct0--;
			}
			act0[num] = state;
			break;
		case 1:
			if (act1[num] != state){
				if (state == stENABLED) nAct1++;
				else nAct1--;
			}
			act1[num] = state;
			break;
		case 2:
			if (actF[num] != state){
				if (state == stENABLED) nActF++;
				else nActF--;
			}
			actF[num] = state;
			break;
	}
}

pair< int, double > greedyPFS::greedyStep(int param, int dim)
{
	double max = -10e8;
	int resNum = -1;

	// check whether it is possible to make a step
	if (
		(param == 1 &&
			(
			(dim == 0 && nAct0 == n0 ) ||
			(dim == 1 && nAct1 == n1 ) ||
			(dim == 2 && nActF == nF)
			)
		) ||
		(param == -1 &&
			(
			(dim == 0 && nAct0 == 0 ) ||
			(dim == 1 && nAct1 == 0 ) ||
			(dim == 2 && nActF == 0)
			)
		))
		return pair<int, double>(-1, 0);

	// pointer to array containing activity information
	bool *act = NULL;
	// size of the array of elements
	int sz;
	switch(dim){
	case 0:
		act = act0; sz = n0; break;
	case 1:
		act = act1; sz = n1; break;
	case 2:
		act = actF;	sz = nF; break;
	}

	for (int num = 0; num < sz; num++)
	if ((act[num] && param == -1) || (!act[num] && param == 1))
	{
		// del step
		if (param == -1){
			// disable the element
			setElementState(dim, num, stDISABLED);
			if (dim == 2)
				changeMatSum(num, -1);
		}
		// add step
		else{
			// enable the element
			setElementState(dim, num, stENABLED);
			if (dim == 2)
				changeMatSum(num, +1);
		}

		double temp = qualityFunc(2);
		if (temp >= max){
			max = temp;
			resNum = num;
		}

		// del step
		if (param == -1){
			// enable the element
			setElementState(dim, num, stENABLED);
			if (dim == 2)
				changeMatSum(num, +1);
		}
		else{
			// disable the element
			setElementState(dim, num, stDISABLED);
			if (dim == 2)
				changeMatSum(num, -1);
		}
	}

	return pair<int, double>(resNum, max);
}

pair< int, double > greedyPFS::greedyDelStep(int dim)
{
	return greedyStep(-1, dim);
}

pair< int, double > greedyPFS::greedyAddStep(int dim)
{
	return greedyStep(1, dim);
}

void initializeActiveBest(bool *&actBest, int n)
{
	actBest = new bool[n];
}

void dropActiveBest(bool *actBest)
{
	delete[] actBest;
}

void greedyPFS::cycle(int mode0, int mode1, int modeF, int parameterD)
{
	int iterBest = iteration;

	// initializing best iteration
	double bestQF = curQF;
	copy(act0, act0 + n0, act0Best);
	copy(act1, act1 + n1, act1Best);
	copy(actF, actF + nF, actFBest);
		
	while (iteration - iterBest < parameterD)
	{
		iteration++;

		// for all 3 dimensions make one step 
		pair< int, double > ans0(-1, -1);
		pair< int, double > ans1(-1, -1);
		pair< int, double > ansF(-1, -1);
		
		if (mode0 == mAdd)
			ans0 = greedyAddStep(0);
		else if (mode0 == mDel)
			ans0 = greedyDelStep(0);

		if (mode1 == mAdd) 
			ans1 = greedyAddStep(1);
		else if (mode1 == mDel)
			ans1 = greedyDelStep(1);

		if (modeF == mAdd)
			ansF = greedyAddStep(2);
		else if (modeF == mDel)
			ansF = greedyDelStep(2);

		// choose the dimension and corresponding element with maximal quality function value
		int resDim = 0;
		int resNum = ans0.first;
		int *mode = &mode0;
		double maxQF = ans0.second;
		if (ans1.second > maxQF){
			resDim = 1; resNum = ans1.first; maxQF = ans1.second;
			mode = &mode1;
		}
		if (ansF.second > maxQF){
			resDim = 2;	resNum = ansF.first; maxQF = ansF.second;
			mode = &modeF;
		}

		// if failed to make a step
		if (resNum == -1) break;
		curQF = maxQF;

		// set state for the chosen element
		if (*mode == mAdd)
		{
			setElementState(resDim, resNum, stENABLED);
			if (resDim == 2)
				changeMatSum(resNum, mAdd);
		}
		else
		{
			setElementState(resDim, resNum, stDISABLED);
			if (resDim == 2)
				changeMatSum(resNum, mDel);
		}
	
		report.push_back(pair<int, int>(resDim, resNum));
		// update nMonotonic and nViolating
		countMonotonicAndViolating();
		pairMonViol.push_back(pair<int, int>(nMonotonic, nViolating));
		pairAlmostMonViol.push_back(pair<int, int>(nAlmostMonotonic, nAlmostViolating));
		action.push_back(*mode);
			
		// update best iteration
		if (curQF >= bestQF){
			bestQF = curQF; iterBest = iteration;
			copy(act0, act0 + n0, act0Best);
			copy(act1, act1 + n1, act1Best);
			copy(actF, actF + nF, actFBest);

			if (curQF > globalBestQF + 1E-6)
			{
				globalBestQF = curQF;
				iterationGlobalBest = iteration;
				copy(act0, act0 + n0, act0GlobalBest);
				copy(act1, act1 + n1, act1GlobalBest);
				copy(actF, actF + nF, actFGlobalBest);
			}
		}
	}

}

void greedyPFS::runGreedyPFS(int parameterD)
{
	// matSum is alredy initialized

	// quality function value at the previous step
	prevQF = -1e10;
	// quality function value at the current step
	curQF = 0;

	globalBestQF = -1000;

	initializeActiveBest(act0Best, n0);
	initializeActiveBest(act1Best, n1);
	initializeActiveBest(actFBest, nF);

	initializeActiveBest(act0GlobalBest, n0);
	initializeActiveBest(act1GlobalBest, n1);
	initializeActiveBest(actFGlobalBest, nF);

	report.resize(0);
	report.reserve(100);
	pairMonViol.resize(0);
	pairMonViol.reserve(100);
	action.resize(0);
	action.reserve(100);

	int iterationGlBest0 = -1;
	// Add-Del algorithm
	iteration = iterationGlobalBest = 0;
	while (curQF >= prevQF && iterationGlBest0 != iterationGlobalBest)
	{
		iterationGlBest0 = iterationGlobalBest;
		prevQF = curQF;

		if (iteration != 0)
			cycle(mDel, mDel, mNothing, parameterD);
		cycle(mNothing, mNothing, mAdd, parameterD);
		cycle(mAdd, mAdd, mNothing, parameterD);
		cycle(mNothing, mNothing, mDel, parameterD);

		/*
		// Add cycle
		cycle(mNothing, mNothing, mAdd, parameterD);
		// Del cycle
		cycle(mNothing, mNothing, mDel, parameterD);
		*/
	}	

	// best structure
	for (int i = 0; i < pHealthyPeople->size(); i++)
		pHealthyPeople->setObjectState(i, act0GlobalBest[i]);
	for (int j = 0; j < pIllPeople->size(); j++)
		pIllPeople->setObjectState(j, act1GlobalBest[j]);
	for (int k = 0; k < ngramm_vector_size; k++)
	{
		pHealthyPeople->setFeatureState(k, actFGlobalBest[k]);
		pIllPeople->setFeatureState(k, actFGlobalBest[k]);
	}

	//drop activeBest
	dropActiveBest(act0Best);
	dropActiveBest(act1Best);
	dropActiveBest(actFBest);

	dropActiveBest(act0GlobalBest);
	dropActiveBest(act1GlobalBest);
	dropActiveBest(actFGlobalBest);
}

void greedyPFS::changeMatSum(int fNum, int mode)
{
	for (int i = 0; i < n0; i++)
		for (int j = 0; j < n1; j++)
			if (T(i, j, fNum)) matSum[i][j] += mode;
}

void greedyPFS::countMonotonicAndViolating(void)
{
	double percent = 0.79;
	nMonotonic = nViolating = 0;
	nAlmostMonotonic = nAlmostViolating = 0;
	nActivePairs = nAct0*nAct1;
	bool *a0 = act0;
	bool *a1 = act1;
	for (int i = 0; i < n0; i++)
	{
		a1 = act1;
		if (*a0)
			for (int j = 0; j < n1; j++)
			{
				if (*a1)
				{
					if (matSum[i][j] == 0)
						nViolating++;
					else
					if (matSum[i][j] == nActF)
						nMonotonic++;
					else
					if (matSum[i][j] <= nActF * (1 - percent))
						nAlmostViolating++;
					else
					if (matSum[i][j] >= nActF * percent)
						nAlmostMonotonic++;
				}
				a1++;
			}
		a0++;
	}
}

greedyPFS::~greedyPFS()
{
	// drop active
	delete[] act0;
	delete[] act1;
	delete[] actF;

	// drop matrix matSum
	for (int i = 0; i < n0; i++)
		delete[] matSum[i];
	delete[] matSum;
}

#include <iomanip>
void greedyPFS::printReport(ofstream &out)
{
	out << "Greedy PFS report" << endl;
	out << "-----------------------------------------------------" << endl;
	out << report.size() << " iterations passed" << endl;
	out << setw(20) << "Action" << setw(20) << "Dimension" << setw(20) << "Num" << setw(20) << 
		"Monotonic" << setw(20) << "nViolating" << setw(20) <<  "nAlmostMonotonic" << setw(20) <<  "nAlmostViolating" << endl;
	for (int i = 0; i < report.size(); i++)
		out << setw(20) << action[i] << setw(20) << report[i].first << setw(20) << report[i].second << setw(20) << 
		pairMonViol[i].first <<  setw(20) << pairMonViol[i].second << setw(20) <<
		pairAlmostMonViol[i].first <<  setw(20) << pairAlmostMonViol[i].second << endl;
	out << endl << endl;
}

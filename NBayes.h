/**********************************************************************************
	* File: NBayes.h
	* Description: Naive Bayes header file
	Class NBayes is an implementation of naive Bayes classifier
	for the case of binary features (for this case the classifier is linear)
	* Created: 16.09.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

	 
***********************************************************************************/

#pragma once

#include "Algorithm.h"
#include "ObjFeatureMatrix.h"

#include <vector>

#define tTreshold 2

class NBayes:public Algorithm
{
private:	
	double **healthyProbabilities;
	double **illProbabilities;

	bool activeFeatures[ngramm_vector_size];

	//probabilities assesment 
	void findDescreteProbabilities(ObjFeatureMatrix &people, double **probabilities);

	//count weights for all features
	void countWeights();

	void resetAll();
public:
	double *ans;

	double weights[ngramm_vector_size];

	NBayes( void );

	virtual void learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople);
	virtual void test(ObjFeatureMatrix &testPeople);
	virtual void report(ofstream *out);
};


/**********************************************************************************
	* File: MonotonicNN.h
	* Description: Monotonic nearest neighbour: given monotonic training sample
	builds model of monotonic nearest neighbour 
	* Created: 23.09.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

#pragma once
#include <vector>
#include <set>
#include <algorithm>

#include "ObjFeatureMatrix.h"
#include "Algorithm.h"

#define rTooManyViolating -100

using namespace std;

class MonotonicNN: public Algorithm
{
private:
	ObjFeatureMatrix healthyPrototypes;
	ObjFeatureMatrix illPrototypes;
	
	// distToShadow -- distance to shadow from person 'from' to person 'to'
	double distToShadow(person& from, person& to, int type);

	// findNNByShadow -- given a person 'current' finds nearest (by shadow) neighbour from the array of people 'people'
	person* findNNByShadow(person &current, ObjFeatureMatrix &prototypes, int type);
	
	// isInShadow -- tells whether person 'a' is in shadow of person 'b'
	bool isInShadowOfB(person &a, person &b);
		
	bool aDominatesB(person &a, person &b);
		
	int nPairs;
	int nMonotonic, nViolating;
	int nThrownOut;

	void copyLearningSample(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople);

	// on each step throws disables object which makes maximal number of violating pairs
	void greedyMonotonization();
	// on each step throws disables object which has maximal value of violating pairs devided by the number of active objects of OTHER class 
	void greedyMonotonization2();
	// alpha percent of ill people forming violating pairs is thrown out
	// after that only healhy people are thrown out
	void greedyMonotonizationAlpha(double alpha);
	// alpha value (for greedyMonotonization)
	double alpha;

	//
	void resetAll();

	// prototypeSelection -- given an array of people 'people'
	// returnes a vector of pointers to prototype people 
	void prototypeSelection();

	double phi(person &a);
	double H(person &a);

public:
	//double *ans;

	MonotonicNN();

	void setAlpha(double a);

	virtual void learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople);

	virtual void test(ObjFeatureMatrix &testPeople);

	virtual void report(ofstream *out);

};







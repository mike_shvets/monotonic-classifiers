#include "NN.h"

double NN::dist(person& from, person& to)
{
	double res = 0;

	for (int k = 0; k < ngramm_vector_size; k++)
	if (healthyPrototypes.getFeatureState(k) == stENABLED)
		res += abs(from[k] - to[k]);

	return sqrt(res);
}

person* NN::findNNByShadow(person &current, ObjFeatureMatrix &prototypes)
{
	double minDist = 1e8;
	person* res = NULL;

	for (int i = 0; i < prototypes.size(); i++)
	if (prototypes.getObjectState(i) == stENABLED)
	{
		double curDist = dist(current, prototypes[i]);
		if (curDist < minDist)
		{
			minDist = curDist;
			res = &prototypes[i];
		}
	}
		return res;
}

void NN::resetAll()
{
	healthyPrototypes.clear();
	illPrototypes.clear();

	if (ans != NULL)
	{
		delete[] ans;
		ans = NULL;
	}
};

void NN::copyLearningSample(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople)
{
	healthyPrototypes.copyFeaturesStates(learnHealthyPeople);
	illPrototypes.copyFeaturesStates(learnIllPeople);

	for (int i = 0; i < learnHealthyPeople.size(); i++)
		if (learnHealthyPeople.getObjectState(i))
			healthyPrototypes.pushBack(learnHealthyPeople[i], learnHealthyPeople.getObjectState(i));
	for (int j = 0; j < learnIllPeople.size(); j++)
		if (learnIllPeople.getObjectState(j))
			illPrototypes.pushBack(learnIllPeople[j], learnIllPeople.getObjectState(j));
}

NN::NN()
{
	ans = NULL;
}

void NN::learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople)
{
	resetAll();
	
	copyLearningSample(learnHealthyPeople, learnIllPeople);
}

void NN::test(ObjFeatureMatrix &testPeople)
{
	if (ans != NULL)
		delete[] ans;
	ans = new double[testPeople.size()];

	for (int i = 0; i < testPeople.size(); i++)
	{
		person* nearestHealthy = findNNByShadow(testPeople[i], healthyPrototypes);
		person* nearestIll = findNNByShadow(testPeople[i], illPrototypes);

		double dH = dist(testPeople[i], *nearestHealthy);
		double dI = dist(testPeople[i], *nearestIll);
		ans[i] = (dH - dI)/(dH + dI);
		/*
		if (dist(testPeople[i], *nearestHealthy) < 
			dist(testPeople[i], *nearestIll))
			ans[i] = Healthy;
		else
			ans[i] = Ill;
			*/
	}
}	




/**********************************************************************************
	* File: Algorithm.h
	* Description: class Algorithm implements an interface for algorithm
	Thus, each algorithm in the project has three methods -- learn(), test() and report() 
	* Created: 01.10.2014
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

#pragma once

#include <iostream>
#include <fstream>

#include "ObjFeatureMatrix.h"

using namespace std;

class Algorithm
{
public:
	double *ans;

	Algorithm(void);
	~Algorithm(void);

	virtual void learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople);
	virtual void test(ObjFeatureMatrix &testPeople);
	virtual void report(ofstream *out);
};


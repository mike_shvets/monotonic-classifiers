#include "MakhinaAlg.h"

int MakhinaAlg::learn(vector < person > &lPeople)
{
	algLearnPeople = lPeople;
	return 0;
}

vector< int > MakhinaAlg::test(vector < person > &algTestPeople)
{
	vector < int > res;
	for (int i = 0; i < algTestPeople.size(); i++)
		res.push_back(findNearestLabel(algTestPeople[i]));

	return res;
}

void MakhinaAlg::report(ofstream *out)
{

}


int MakhinaAlg::findNearestLabel(person &p)
{
	int res = -1;
	int minDist = 1000000;
	for (int i = 0; i < algLearnPeople.size(); i++)
	{
		int curDist = MDist(p, algLearnPeople[i]);
		if (curDist < minDist)
		{
			minDist = curDist;
			res = algLearnPeople[i].getLabel();
		}
	}

	return res;
}

double MakhinaAlg::MDist(person &a, person &b)
{
	int n = activeFeaturesList.size();
	int k = 50;

	double distAB = 0;
	for (vector< int >::iterator it = activeFeaturesList.begin();
			it < activeFeaturesList.end(); it++)
	{
		distAB += abs(a.features[*it] - b.features[*it]);
	}
	double res = n*k*distToShadow(a,b) + (n*k - distAB); 

	return res;
}

double MakhinaAlg::distToShadow(person& from, person& to)
	{
		double res = 0;
		//for (int j = 0; j < from.getFeatureCount(); j++)
		//if (person::isActiveFeature[j])
		for (vector< int >::iterator it = activeFeaturesList.begin();
			it < activeFeaturesList.end(); it++)
		{
			if (to.getLabel() == 0 && from.features[*it] > to.features[*it])
				res += from.features[*it] - to.features[*it];
			if (to.getLabel() == 1 && from.features[*it] < to.features[*it])
				res += to.features[*it] - from.features[*it];
		}
		return res;
	}
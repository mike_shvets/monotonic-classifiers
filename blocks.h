#pragma once

//#include <iostream>
//#include <fstream>
#include <algorithm>

//#include "dataTransformation.h"
//#include "person.h"
//#include "crossValidation.h"
#include "NBayes.h"
#include "ObjFeatureMatrix.h"

#define mGetWeightsBayes 1
#define mGetWeightsMediumFreq 2
#define mGetWeightsMediumFreqDiff 3
#define mGetWeightsOccurrence 4
#define mGetWeightsOccurrenceDiff 5
#define mGetWeightsOccurrenceDiffAbs 6
#define mGetWeightsMediumFreqDiffAbs 7

using namespace std;

// multiplies by -1 those features, for which the median value of ill people is LESS than the median value of healthy people
// turns off those features, for which the median values differ no more than treshold value  
vector< bool > doChangeSigns(ObjFeatureMatrix& healthyPeople, ObjFeatureMatrix& illPeople, double treshold, bool printReport = 0);

// returns vector of pairs (iFeature, iFeature_weight)
// where iFeature_weight is the weight of iFeature counted in mode mGetWeights 
vector< pair< int, double > > getWeights(ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople, int mGetWeights, bool printWeightsVector = false);

void doPreliminaryFeatureSelection(int nFeatures, vector< pair< int, double > >& resBayes,
						ObjFeatureMatrix &healthyPeople, ObjFeatureMatrix &illPeople);
/*
void printClassificationReport(crossValidation& CV);

*/






/**********************************************************************************
	* File: ObjFeatureMatrix.h
	* Description: class ObjFeatureMatrix implemented to store an array of instances of class person
	* Created: 03.01.2015
	* Author: Mikhail
	* Email: mikhail.shvets@phystech.edu

***********************************************************************************/

#pragma once
#include <vector>

#include "person.h"

#define stDISABLED false
#define stENABLED true

class ObjFeatureMatrix
{
private:
	// vector of people
	vector< person > people;
	// number of people
	int _size;
	
	// number of active features
	int nActiveFeatures;
	// pointer to an array holding feature states (stEnabled or stDisabled)
	bool *featureState;
	// number of active objects
	int nActiveObjects;
	// pointer to an array holding objects states
	vector< bool > objectState;

	void activateAllObjects(void);
	void activateAllFeatures(void);

public:
	void disableAllObjects(void);
	void disableAllFeatures(void);

	// constructor
	ObjFeatureMatrix(void);
	// constructor with a name of file to read from
	ObjFeatureMatrix(char *fileName, int sLabel);
	// destructor
	~ObjFeatureMatrix(void);

	// gets the _size
	int size(void);
	// pushes oneMore person to the end of the vector people
	void pushBack(person oneMore, bool state = stENABLED);
	void clear();

	void copyFeaturesStates(ObjFeatureMatrix &M);

	// gets the person with number index
	person& operator[](int index);

	// gets number of active objects
	int numberOfActiveObjects( void );
	// get object state
	bool getObjectState(int iObj);
	// set object (with number iObj) state to state
	void setObjectState(int iObj, bool state);
	// gets number of active features
	int numberOfActiveFeatures( void );
	// get feature state
	bool getFeatureState(int iFeature);
	// set feature (with number iFeatures) state to state
	void setFeatureState(int iFeature, bool state);
};


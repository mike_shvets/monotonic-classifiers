#include "NBayes.h"

#define eps 10e-8

int bin(double t)
{
	if (t < -eps)
	{
		if (t <= -tTreshold - 0.5 + eps)
			return 0;
		else
			return 1;
	}
	else
	{
		if (t >= tTreshold - eps)
			return 1;
		else
			return 0;
	}
}

void NBayes::findDescreteProbabilities(ObjFeatureMatrix &people, double **probabilities)
{
	for (int j = 0; j < ngramm_vector_size; j++)
		for (int t = 0; t < 2; t++)
			probabilities[j][t] = 1;

	for (int j = 0; j < ngramm_vector_size; j++)
      for (int i = 0; i < people.size(); i++)
		  probabilities[j][bin(people[i][j])]++;

	for (int j = 0; j < ngramm_vector_size; j++)
      for (int t = 0; t < 2; t++)
         probabilities[j][t] /= people.size() + 2;
}

void NBayes::countWeights()
{
	for (int j = 0; j < ngramm_vector_size; j++)
	{
		if ( abs(healthyProbabilities[j][1]) < eps || abs( illProbabilities[j][0] ) < eps)
			throw "Error while counting bayesian weights";
		weights[j] = log((illProbabilities[j][1]/healthyProbabilities[j][1])*(healthyProbabilities[j][0]/illProbabilities[j][0]));
	}
}

void NBayes::resetAll()
{
	if (ans != NULL)
		delete[] ans;
	ans = NULL;

	for (int j = 0; j < ngramm_vector_size; j++)
		weights[j] = 0;

	for (int k = 0; k < ngramm_vector_size; k++)
		activeFeatures[k] = stENABLED;
}

NBayes::NBayes( void )
{
	ans = NULL;

	healthyProbabilities = new double*[ngramm_vector_size];
	illProbabilities = new double*[ngramm_vector_size];
	for (int k = 0; k < ngramm_vector_size; k++)
	{
		healthyProbabilities[k] = new double[2];
		illProbabilities[k] = new double[2];
	}
}

void NBayes::learn(ObjFeatureMatrix &learnHealthyPeople, ObjFeatureMatrix &learnIllPeople)
{
	resetAll();

	findDescreteProbabilities(learnIllPeople, illProbabilities);
	findDescreteProbabilities(learnHealthyPeople, healthyProbabilities);

	countWeights();

	for (int k = 0; k < ngramm_vector_size; k++)
		if (learnHealthyPeople.getFeatureState(k) == stDISABLED)
			activeFeatures[k] = stDISABLED;
		else
			activeFeatures[k] = stENABLED;
}

//TODO: 
void NBayes::test(ObjFeatureMatrix &testPeople)
{
	ans = new double[testPeople.size()];

	for (int i = 0; i < testPeople.size(); i++)
	{
		ans[i] = 0;
		for (int k = 0; k < ngramm_vector_size; k++)
			if (testPeople.getFeatureState(k))
				ans[i] += bin(testPeople[i][k]) * weights[k];
	}
}

void NBayes::report(ofstream *out)
{
	*out << "***Naive Bayes section***" << endl << endl;
}

